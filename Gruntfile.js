module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    clean: ['dist/*.js', 'test/testem.tap'],
    jshint: {
      all: ['src/*.js'],
      options: grunt.file.readJSON('.jshintrc')
    },
    protractor: {
      options: {
        configFile: 'conf.js',
        keepAlive: true,
        noColor: false,
        args: {}
      },
      run_tests: {
        options: {
          configFile: 'conf.js',
          args: {}
        }
      }
    },
    concat: {
      build: {
        files: {
          'dist/<%= pkg.name %>.js': [
            'src/spec.js'
          ]
        }
      }
    },
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
      },
      build: {
        src: 'dist/<%= pkg.name %>.js',
        dest: 'dist/<%= pkg.name %>.min.js'
      }
    },
    "qunit-cov": {
      test: {
        minimum: 0.9,
        srcDir: 'src',
        depDirs: ['test'],
        outDir: 'dist/cov',
        testFiles: ['test/*.html']
      }
    },
    plato: {
      options: {
        title: 'Awesome Project',
        jshint: grunt.file.readJSON('.jshintrc')
      },
      metrics: {
        files: {
          'dist/metrics': [ 'src/*.js' ]
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-protractor-runner');
  grunt.loadNpmTasks('grunt-qunit-cov');
  grunt.loadNpmTasks('grunt-plato');

  // Default task(s).
  grunt.registerTask('default', ['jshint', 'protractor:run_tests', 'clean', 'qunit-cov']);
  grunt.registerTask('jenkins', ['jshint', 'protractor:run_tests', 'clean', 'qunit-cov', 'plato', 'concat', 'uglify']);

};
