var HtmlReporter = require('protractor-html-screenshot-reporter');
var path = require('path');

exports.config = {
  seleniumAddress: 'http://localhost:4444/wd/hub',
  specs: ['src/spec.js'],
  capabilities: {
    name: 'Chrome',
    browserName: 'chrome'
  },

  onPrepare: function() {
    var directory = __dirname;
    console.log(directory);
    jasmine.getEnv().addReporter(new HtmlReporter({
      baseDirectory: directory + '/reports',
      preserveDirectory: true
    }));
  }
}
